public class Student extends Person {
    private static int naechsteMatrNr = 9999;
    private int matrNr;
    private int semester;


    public Student(String name, String vorname, int semester) {
        super(name, vorname);
        this.semester = semester;
        this.email = "";
        this.matrNr = getNaechsteMatrNr();
    }
    
    public static int getNaechsteMatrNr() {
        naechsteMatrNr++;
        return naechsteMatrNr;
    }

    public int getSemester() {
        return this.semester;
    }


    public void setSemester(int semester) {
        this.semester = semester;
    }

    public boolean hatUrlaub() {
        return false;
    }

    public void drucken() {
        //System.out.printf("StudentIn %s ist im %s. Semester.\n", this.name, this.semester);
        
        System.out.printf("%s %s\n", this.vorname, this.name);
        System.out.printf("E-Mail-Adresse: %s\n", this.email);
        System.out.printf("Matrikelnummer %s\n", this.matrNr);
        System.out.printf("im %s. Semester.\n", this.semester);
    }
}