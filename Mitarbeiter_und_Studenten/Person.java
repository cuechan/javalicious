abstract public class Person implements Druckbar {

    String name;
    String vorname;
    String email;

    public Person(String name, String vorname) {
        this.name = name;
        this.vorname = vorname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    abstract boolean hatUrlaub();

    public String getName() {
        return this.name;
    }

    public abstract void drucken();
}