public class Angestellter extends Person {
    private double gehalt;
    private String abteilung;


    public Angestellter(String name, String vorname) {
        super(name, vorname);
        this.email = "";
    }


    public double getGehalt() {
        return this.gehalt;
    }
    
    public void setAbteilung(String bezeichnung) {
        this.abteilung = bezeichnung;
    }

    public boolean hatUrlaub() {
        return false;
    }


    public void drucken() {
        System.out.printf("%s %s\n", this.vorname, this.name);
        System.out.printf("E-Mail-Adresse: %s\n", this.email);
        System.out.printf("arbeitet in Abteilung %s.\n", this.abteilung);
    }
}