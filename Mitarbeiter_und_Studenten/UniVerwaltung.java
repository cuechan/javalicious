/**
 * Klasse   UniVerwaltung
 * @author  Ingrid Schumacher
 * @version 1.0 vom 2018-01-14
 */
public class UniVerwaltung {
    public static void main( String[] args) {
        // Erzeugen eines Arrays fuer drei Personen
        Person[] personen = new Person[3];

        // Erzeugen eines Studenten - Objektes
        Student ingrid = new Student ("Schumacher", "Ingrid", 1);
        // als erste Person in das Array eintragen
        personen[0] = ingrid;

        // Erzeugen eines Angestellten - Objektes
        Angestellter hugo = new Angestellter ("Fleissig", "Hugo");
        // Zuordnen einer Abteilung
        hugo.setAbteilung ("ITSC");
        // Zuordnen der E-Mail
        hugo.setEmail ("fleissig@gmx.de");
        // als zweite Person in das Array eintragen
        personen[1] = hugo;

        // Erzeugen eines Studenten - Objektes
        personen[2] = new Student ("Sorglos ", "Emil", 8);

        if (ingrid.hatUrlaub()) {
            System.out.println("StudentIn " + ingrid.getName() + " hat ein Urlaubssemester.");
        }
        else {
        System.out.println("StudentIn " + ingrid.getName() + " ist im " + ingrid.getSemester() + ". Semester.");
        }

        // Ausgabe aller Personen an der Konsole
        for (int i = 0; i < personen.length; i++) {
            personen[i].drucken();
        }

        // Anzahl der eingetragenen Studenten
        int anzahlStudenten = 0;
        for (int i = 0; i < personen.length; i++) {
            if(personen[i] instanceof Student) {
                anzahlStudenten++;
            }
        }

        System.out.println("Es sind " + anzahlStudenten + " Studenten eingeschrieben.");

        // Ausgabe der naechsten freien Matrikelnummer
        System.out.println("Naechste Matrikelnummer : " + Student.getNaechsteMatrNr());
    }
}
  