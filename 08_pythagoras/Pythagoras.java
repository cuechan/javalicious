import java.lang.Math;
import java.util.Scanner;

public class Pythagoras {
    public static double squareRoot(double a) {
        return Math.sqrt(a);
    }

    public static void main(String[] args) {
        // Variablen deklarieren
        double a = 0.0;
        double b = 0.0;
        double hypothenuseC = 0.0;

        // Lese Laenge der Katheten ein
        Scanner scanner = new Scanner(System.in);
        a = scanner.nextInt();
        b = scanner.nextInt();
        scanner.close();
    
        // *** Fuegen Sie hier Ihre Loesung ein:
        
        hypothenuseC = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));

        // Gib das berechnete Ergebnis aus
        System.out.println(hypothenuseC);
    }
}