import java.util.ArrayList;

/**
 * Weihnachtswald
 */
public class Weihnachtswald {
	private ArrayList<Weihnachtsbaum> forest;

	public Weihnachtswald() {
		this.forest = new ArrayList<>();
	}

	/**
	 * plant a new tree
	 * @param height height of tree
	 */
	public void pflanze(int height) {
		forest.add(new Weihnachtsbaum(height));
	}


	/**
	 * Draw all the whole forest on screen
	 */
	public void zeichne() {
		for (Weihnachtsbaum tree: this.forest) {
			tree.draw();
		}
	}

}
