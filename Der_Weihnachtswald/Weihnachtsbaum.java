/**
 * Weihnachtsbaum
 */
public class Weihnachtsbaum {
	private int height;

	/**
	 * create a new tree with specific height
	 * @param height
	 */
	public Weihnachtsbaum(int height) {
		this.height = height;
	}

	/**
	 * draw the tree
	 */
	public void draw() {
		String out = new String();

		for (int i = 0; i < height; i++) {
			// out += i+"|";
			for (int j = 1; j < height-i; j++) {
				out += " ";
			}

			for (int j = 0; j < (i*2)+1; j++) {
				out += "*";
			}

			out += "\n";
		}

		int snag = 2*((int) (height/4))+1;

		for (int i = 0; i < snag; i++) {
			for (int j = 0; j < ((height*2)-snag)/2; j++) {
				out += " ";
			}

			for (int j = 0; j < snag; j++) {
				out += "*";
			}

			out += "\n";
		}

		System.out.print(out);
	}
}
