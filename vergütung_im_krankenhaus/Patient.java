public abstract class Patient{

	private String name;
	private int krankheitsklasse;


	public Patient(String name, int krankheitsklasse){
		this.name = name;
		this.krankheitsklasse = krankheitsklasse;
	}

	public abstract void zeigePatient();

	public String getName(){
		return name;
	}

	public double getVerguetung() {
	   switch (krankheitsklasse) {
	       case 1:
	           return 150;
	       case 2:
	           return 500;
	       case 3:
	           return 1000;
	   }
	   return 0;
	}
}
