public class Erwachsener extends Patient {

    public Erwachsener(String name, int krankheitsklasse) {
        super(name, krankheitsklasse);
    }

    public double getVerguetung() {
        return super.getVerguetung();
    }

    public void zeigePatient() {
        System.out.println(super.getName() + " (Erwachsener)");
    }
}
