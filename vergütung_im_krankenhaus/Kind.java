/*
 * Klasse zur Modellierung eines Kindes
 *
 * Ist von Ihnen anzupassen
 */

class Kind extends Patient {
    private int alter;


    public Kind(String name, int krankheitsklasse, int alter) {
        super(name, krankheitsklasse);
        this.alter = alter;
    }


    public double getVerguetung() {
        return 1.25 * super.getVerguetung();
    }


    public void zeigePatient() {
        System.out.println(super.getName() + " (Kind, " + alter + " Jahre)");
    }
}
