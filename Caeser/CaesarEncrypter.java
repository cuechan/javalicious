

public class CaesarEncrypter implements Encrypter {
    private String alphabet = "abcdefghijklmnopqrstuvwxyz .,!?";
    private int shift; 

    public CaesarEncrypter(int shift) {
        this.shift = shift;
    }

    public String encrypt(String message) {
        String res = "";

        for (int i = 0; i < message.length(); i++) {
            char c = message.charAt(i);

            int index = this.alphabet.indexOf(c);
            int newindex =  (this.shift + index) % this.alphabet.length();
            
            res += this.alphabet.charAt(newindex);
        }


        return res;
    }
   
    public String decrypt(String message) {
        String res = "";

        for (int i = 0; i < message.length(); i++) {
            char c = message.charAt(i);

            int index = this.alphabet.indexOf(c);
            System.out.println("old index is: " + index);
            
            int newpos = index + (this.alphabet.length() - (this.shift % alphabet.length()));
            System.out.println("new position is: "+newpos);
            
            newpos = newpos % this.alphabet.length(); 
            res += this.alphabet.charAt(newpos);
        }

        return res;
    }
}
