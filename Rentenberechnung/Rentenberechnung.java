import java.util.Scanner;


public class Rentenberechnung {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        // getting the sparbetrag from stdin
        double sparbetrag = sc.nextDouble();
        // how long until i can finally rest
        int dauer = (int) sc.nextDouble();
        // read zins from stdin
        double zins = sc.nextDouble();

        sc.close();

        // credit in cents
        double kontostand = 0;

        
        for (int i = 0; i < dauer; i++) {
            // add value to bank account
            kontostand += sparbetrag;

            // calculate interest yield
            double yield = kontostand * (zins / 100);
            // round the yield to two digits
            yield = ((double) Math.round(yield * 100)) / 100;

            // round again because... well its Java
            kontostand = (double) Math.round((kontostand + yield) * 100) / 100;
        }

        // richkid
        System.out.println(kontostand);
    }
}