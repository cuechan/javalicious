import java.util.*;

public class ICE extends Zug implements MitBordbistro {

    public ICE (String linie, ArrayList<String> haltestellen) {
        super(linie, haltestellen);
    }
    
    public boolean istLangstrecke() {
        return (super.getHaltestellen().size() > 10) ? true : false;
    }
    
    public int getVermutlicheVerspaetung(String haltestelle) {
        int i = 0;
        for(String s : super.getHaltestellen()) {
            if(s == haltestelle) {
                return i*2;
            }i++;
        }
        return 0;
        //return super.getHaltestellen().size();
    }
    
    public String getRestaurantSpezialitaet() {
        for(String name: super.getHaltestellen()) {
            if(name == "Lübeck") {
                return "Marzipan";
            }
        }
        return "Brezeln";
    }
    
    public int getAnzahlSpeiseplaetze() {
        return 40;
    }
}