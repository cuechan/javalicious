import java.util.*;

public abstract class Zug {
    private ArrayList<String> haltestellen = new ArrayList<String>();
    private String linie;
    
    public Zug (String linie, ArrayList<String> haltestellen) {
        this.linie = linie;
        this.haltestellen = haltestellen;
    }
    
    public abstract int getVermutlicheVerspaetung(String haltestelle);
    
    protected ArrayList<String> getHaltestellen() {
        return this.haltestellen;
    }
    
    public String getLinie() {
        return this.linie;
    }
    
    public void verlaengere(String neueHaltestelle) {
        haltestellen.add(neueHaltestelle);
    }
}