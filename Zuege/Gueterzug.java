import java.util.*;

public class Gueterzug extends Zug {
    
    private static String gefahrenladung;
    public String ladung;
    
    public Gueterzug (String linie, String ladung, String von, String zu) {
        super(linie, new ArrayList<String>(Arrays.asList(von, zu)));
        this.ladung = ladung;
        this.gefahrenladung = "brennstaebe";
    }
    
    public static void setGefahrenladung(String name) {
        gefahrenladung = name;
    }
    
    public static String getGefahrenladung() {
        return gefahrenladung;
    }
    
    public int getVermutlicheVerspaetung(String haltestelle) {
        int verspaetung = 0;
        if(this.gefahrenladung == "brennstaebe") {
            verspaetung += 1440; 
        }
        if(haltestelle == "Gorleben") {
            verspaetung += 1440; 
        }
        return verspaetung;
    }
    
    public void verlaengere(String Haltestelle) {}
}