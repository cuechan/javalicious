import java.util.Scanner;

public class GaussStart {
    public static void main(String[] args) {
        // Erstelle einen Scanner und lies eine Zahl n ein.
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        // *** Fuegen Sie hier Ihre Loesung ein.
        // *** Bearbeiten Sie auch die Datei GaussBerechner.java!


        GaussBerechner gauss = new GaussBerechner();
        
        System.out.println("summeFormel \n" + gauss.summeFormel(n));
        System.out.println("summeFor \n" + gauss.summeFor(n));
        System.out.println("summeWhile \n" + gauss.summeWhile(n));
        System.out.println("summeDoWhile \n" + gauss.summeDoWhile(n));

    }
}