/**
 * GaussBerechner
 */
public class GaussBerechner {
    int summeFormel(int n) {
        return (n*(n+1)) / 2;
    }

    int summeFor(int n) {
        int res = 0;
        for (int i = 1; i <= n; i++) {
            res += i;
        }

        return res;
    }

    int summeWhile (int n) {
        int i = 0;
        int res = 0;

        while (i++ < n) {
            res += i;
        }

        return res;
    }

    int summeDoWhile(int n) {
        int res = 0;
        int i = 0;

        do {
            res += i;
        } while (++i <= n);

       return res;
       
    }
}