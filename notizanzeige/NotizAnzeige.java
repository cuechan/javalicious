public class NotizAnzeige {
	private String[] notizen = new String[10];
	private int k = 0;


	public void speichereNotiz(String note) {
		if (k < 10){
			notizen[k] = note;
			k++;
		}
	}


	public void zeigeNotizen() {
		for(int i = 0; i < k; i++) {
			System.out.println(notizen[i]);
		}
	}


	public void loescheAlle() {
		for(int i = 0; i < 10; i++){
			notizen[i] = null;
		}

		k = 0;
	}


	public int getAnzahl() {
		return k;
	}


	public int getLaenge(int i) {
		return notizen[i].length();
	}


	public boolean istSortiert(int i) {
		if (i+1 >= k) {
			return true;
		}
		return (getLaenge(i) < getLaenge(i+1));
	}


	public boolean alleSortiert() {
		for(int i = 0; i < k ; i++){
			if (!istSortiert(i)) {
				return false;
			}
		}
		return true;
	}
}
