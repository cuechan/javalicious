import java.util.Scanner;

class MonatIf {
    public static void main(String[] args) {
        // Variablen deklarieren
        int nm;
        String monat = "";

        // Werte einlesen
        Scanner scanner = new Scanner(System.in);
        nm = scanner.nextInt();
        scanner.close();

        
        // *** Fuegen Sie hier Ihre Loesung ein:
        
        System.out.println(nm);
        if(nm == 1) {
            monat = "Januar";
        }
		if(nm == 2) {
            monat = "Februar";
		}
        if(nm == 3) {
            monat = "März";
		}
        if(nm == 4) {
            monat = "April";
		}
        if(nm == 5) {
            monat = "Mai";
		}
        if(nm == 6) {
            monat = "Juni";
		}
        if(nm == 7) {
            monat = "Juli";
		}
        if(nm == 8) {
            monat = "August";
		}
        if(nm == 9) {
            monat = "September";
		}
        if(nm == 10) {
            monat = "Oktober";
		}
        if(nm == 11) {
            monat = "November";
		}
        if(nm == 12) {
            monat = "Dezember";
        }
        
        
        if(nm > 12) {
            System.out.println("Kein gültiger Monat.");
        }
        else {
            System.out.println(monat);
        }
    }
}

