import java.util.Scanner;

class MonatIfElseIf {
    public static void main(String[] args) {
        // Variablen deklarieren
        int nm;
        String monat = "";

        // Werte einlesen
        Scanner scanner = new Scanner(System.in);
        nm = scanner.nextInt();
        scanner.close();

        // *** Fuegen Sie hier Ihre Loesung ein:
        System.out.println(nm);

        if(nm == 1) {
            monat = "Januar";
        }
		else if(nm == 2) {
			monat = "Februar";
		}
        else if(nm == 3) {
			monat = "März";
		}
        else if(nm == 4) {
			monat = "April";
		}
        else if(nm == 5) {
			monat = "Mai";
		}
        else if(nm == 6) {
			monat = "Juni";
		}
        else if(nm == 7) {
			monat = "Juli";
		}
        else if(nm == 8) {
			monat = "August";
		}
        else if(nm == 9) {
			monat = "September";
		}
        else if(nm == 10) {
			monat = "Oktober";
		}
        else if(nm == 11) {
			monat = "November";
		}
        else if(nm == 12) {
			monat = "Dezember";
		}
        else {
            System.out.println("Kein gültiger Monat.");
            System.exit(1);
        }

        System.out.println(monat);
    }
}

