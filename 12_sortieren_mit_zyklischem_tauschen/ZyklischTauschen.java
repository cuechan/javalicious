import java.util.Scanner;

class ZyklischTauschen {
    public static void main(String[] args) {
        // Deklariere ein Array mit 3 Zeichenketten.
        String[] nachnamen = new String[3];
        
        // Lese 3 Namen ein und speichere sie in dem Array.
        Scanner scanner = new Scanner(System.in);
        nachnamen[0] = scanner.nextLine();
        nachnamen[1] = scanner.nextLine();
        nachnamen[2] = scanner.nextLine();

        // *** Fuegen Sie hier Ihre Loesung ein:
        if(nachnamen[0].compareTo(nachnamen[1]) > 0) {
            String tmp = nachnamen[0];
            nachnamen[0] = nachnamen[1];
            nachnamen[1] = tmp;
        }
        if(nachnamen[1].compareTo(nachnamen[2]) > 0) {
            String tmp = nachnamen[1];
            nachnamen[1] = nachnamen[2];
            nachnamen[2] = tmp;
        }
        if(nachnamen[0].compareTo(nachnamen[1]) > 0) {
            String tmp = nachnamen[0];
            nachnamen[0] = nachnamen[1];
            nachnamen[1] = tmp;
        }

        for (String AAaaaahhhhh : nachnamen) {
            System.out.println(AAaaaahhhhh);
        }
        System.out.println("sortiert");
    }
}
