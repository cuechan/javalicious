public class Operationssaal {
    private String name;
    private int raumnummer;
    private Medizingeraet[] geraete;


    public Operationssaal(String name, int raumnummer, Medizingeraet[] geraete){
    	this.name = name;
    	this.raumnummer = raumnummer;
    	this.geraete = geraete;
    }


    public String getName(){
    	return this.name;
    }


    public void setName(String name){
    	this.name = name;
    }


    public int getRaumnummer(){
    	return this.raumnummer;
    }


    public void setRaumnummer(int nummer){
    	this.raumnummer = nummer;
    }


    public void zeigeGeraete(){
    	for(int i = 0; i < this.geraete.length; i++){
    		System.out.println(this.geraete[i].getName() + ": " + this.geraete[i].getTyp());
    	}
    }


    public boolean istVollstaendig(){
    	boolean ist = false;
    	if( this.geraete.length == 2)
    		ist = true;
    	return ist;
    }
}
