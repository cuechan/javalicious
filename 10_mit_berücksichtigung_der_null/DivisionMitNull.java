public class DivisionMitNull {

    // diese methode ist selbsterklärend
    public void teilenMitNull(double divident, double divisor) {
        if (divisor == 0) {
            System.out.println("Division durch 0 nicht möglich.");
            return;
        }
        
        System.out.println(divident/divisor);
    }
}