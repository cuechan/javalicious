import java.util.Scanner;

class SummeBisN {
    public static void main(String[] args) {
        // Variablen deklarieren
        int n = 0;
        int summe = 0;

        // Eingabe einlesen
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        scanner.close();

        // *** Fuegen Sie hier Ihre Loesung ein:
        

        summe = (n * (n + 1) / 2);

        System.out.println(summe);

    }
}
