public interface Produkt {


    /**
     * Print information and status about a Produkt.
     */
    public void print();

    /**
     * Run this product through a press
     * @param pressfaktor
     */
    public void druecke(double pressfaktor);
}
