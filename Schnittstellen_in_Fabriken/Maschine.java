public interface Maschine {
    /**
     * A Maschine can do work on a Produkt
     * @param produkt
     */
    public void verarbeite(Produkt produkt);
}
