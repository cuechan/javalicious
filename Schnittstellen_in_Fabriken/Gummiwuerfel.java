import java.text.DecimalFormat;

public class Gummiwuerfel implements Produkt {
    double breite, hoehe, laenge;
    private double experiencedForce;

    /**
     * Create a new Gummiwürfel.
     * @param seitenlaenge
     */
    public Gummiwuerfel(double seitenlaenge) {
        this.laenge = seitenlaenge;
        this.hoehe = seitenlaenge;
        this.breite = seitenlaenge;
    }

    /**
     * Put the Würfel into a press and press it!
     * @param pressfaktor
     */

    public void druecke(double pressfaktor) {
        this.experiencedForce += pressfaktor;
        int toPress = (int) this.experiencedForce / 5;

        if (toPress > 0) {
            this.hoehe *= (0.5 * toPress);
            experiencedForce -= 5;
        }
    }

    public void print() {
        System.out.format("Gummiwürfel: %.2fx%.2fx%.2f\n", laenge, breite, hoehe);
    }
}
