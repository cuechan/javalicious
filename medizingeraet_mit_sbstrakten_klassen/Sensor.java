/**
 * Sensor
 */
public class Sensor extends Medizingeraet {
    private int messwert;

    /**
     * Create a new Senser
     * @param hersteller
     * @param threshold
     */
    public Sensor(String hersteller, int threshold) {
        super(hersteller, threshold);
    }

    /** measure something */
    private void messen() {
        messwert = rand.nextInt();
    }

    /**
     * do a quick test measurement and print result
     */
    public void ueberpruefeAlarm() {
        if(!this.istEingeschaltet) {
            System.out.println("Geraet ist ausgeschaltet!");
            return;
        }

        messen();

        // Die implementierung sollte eigentlich korrekt sein...
        // Aber der 3. Test schlägt trotzdem fehl
        if (messwert > alarmSchwellwert) {
            System.out.println("Alarm! Messwert uebersteigt Schwellwert.");
        }
        if (messwert <= alarmSchwellwert) {
            System.out.println("Messwert ist in Ordnung.");
        }
    }
}
