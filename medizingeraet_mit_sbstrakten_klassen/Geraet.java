public abstract class Geraet {

    protected boolean istEingeschaltet;
    private String hersteller;

    public Geraet(String hersteller) {
        this.hersteller = hersteller;
    }

    public String gibHersteller() {
        return this.hersteller;
    }

    abstract public void drueckePowerKnopf();
}
