/**
 * Complex
 */
public class Complex extends SimpleComplex implements Nat, Konjugierbar<Complex> {

	public Complex(double real, double imaginary) {
		super(real, imaginary);
	}


	public Complex konjugiere() {
		return new Complex(getReal(), -getImaginary());
	}


	public Complex addiere(Nat summand) {
		return new Complex(getReal()+summand.getNatWert(), getImaginary());
	}

	public long getNatWert() {
	    if (this.getReal() < 0) {
	        return 0;
	    }
		return Math.round(getReal());
	}
}
