/**
 * SimpleComplex is a simple (hence the name) representation for complex
 * numbers. It only supports creation and output as a string.
 *
 * @author Florian Buether
 * @version 1.0
 */
public class SimpleComplex {
	/** The real part of this complex number. */
	private final double real;

	/** The imaginary part of this complex number. */
	private final double imaginary;

	/**
	 * Create a new complex number.
	 *
	 * @param real      the real part of the complex number.
	 * @param imaginary the imaginary part.
	 */
	public SimpleComplex(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}

	/**
	 * Determines the real part of this complex number.
	 *
	 * @return The real part of this complex number.
	 */
	double getReal() {
		return real;
	}

	/**
	 * Determines the imaginary part of this complex number.
	 *
	 * @return The imaginary part of this complex number.
	 */
	double getImaginary() {
		return imaginary;
	}

	/**
	 * Get the typical mathematical representation of a complex number, i.e. in form
	 * a + bi.
	 *
	 * @return A string representation of this complex number.
	 */
	public String toString() {
		return real + "+" + imaginary + "i";
	}
}
