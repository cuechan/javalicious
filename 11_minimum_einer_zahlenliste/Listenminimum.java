import java.util.Scanner;

class Listenminimum {
    public static void main(String[] args) {
        // Lese die Anzahl der Zahlen ein
        Scanner scanner = new Scanner(System.in);
        int anzahl = scanner.nextInt();
        
        // Deklariere ein Array der passenden Groesse
        int[] zahlen = new int[anzahl];
        
        // Lese die einzelnen Zahlen ein
        for (int i = 0; i < anzahl; i++) {
            zahlen[i] = scanner.nextInt();
        }
        scanner.close();
        
        // Deklariere Variable fuer das Minimum
        int minimum;

        // *** Fuegen Sie hier Ihre Loesung ein:

        minimum = zahlen[0];


        for (int i : zahlen) {
            if (i < minimum) {
                minimum = i;
            }
        }


        // Ergebniss ausgeben
        System.out.println(minimum);
    }
}