import java.util.Scanner;

class MonatSwitch {
    public static void main(String[] args) {
        // Variablen deklarieren
        int nummerMonat;
        String monat = "";

        // Werte einlesen
        Scanner scanner = new Scanner(System.in);
        nummerMonat = scanner.nextInt();
        scanner.close();

        // *** Fuegen Sie hier Ihre Loesung ein:
        System.out.println(nummerMonat);
        switch (nummerMonat) {
            case 1:
                monat = "Januar";
                break;
            case 2:
                monat = "Februar";
                break;
            case 3:
                monat = "März";
                break;
            case 4:
                monat = "April";
                break;
            case 5:
                monat = "Mai";
                break;
            case 6:
                monat = "Juni";
                break;
            case 7:
                monat = "Juli";
                break;
            case 8:
                monat = "August";
                break;
            case 9:
                monat = "September";
                break;
            case 10:
                monat = "Oktober";
                break;
            case 11:
                monat = "November";
                break;
            case 12:
                monat = "Dezember";
                break;
            default:
                System.out.println("Kein gültiger Monat.");
                System.exit(1);
        }
        
        System.out.println(monat);
    }
}