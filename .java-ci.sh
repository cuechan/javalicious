#!/bin/bash

set -e

for i in `find ./ -maxdepth 1 -type d \! -name .\*`; do
	cd $PWD
	echo "===== $i =====";

	javac -verbose ${i}/*.java

	echo "everything went fine"
	echo 
done
